package com.example.demo;

import java.util.Random;

public class Entity {
	
	private int phoneNum;
	private int OTP;
	
	
	public Entity(int phoneNum) {
		this.phoneNum = phoneNum;
		this.OTP = Math.abs(new Random().nextInt());
	}


	public int getPhoneNum() {
		return phoneNum;
	}


	public void setPhoneNum(int phoneNum) {
		this.phoneNum = phoneNum;
	}


	public int getOTP() {
		return OTP;
	}


	public void setOTP(int oTP) {
		OTP = oTP;
	}
	
	
}
