package com.example.demo;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@Autowired
	 private Service service;
	
	@GetMapping("/api")
	public String test(){
		return "Working";
	}
	
	@GetMapping("/api/all")
	public List getAll() {
		return service.getAll();
	}
	
	@GetMapping("/api/{phoneNum}")
	public int getOTP(@PathVariable int phoneNum) {
		return service.getOTP(phoneNum);
	}
	
	@GetMapping("/api/{phoneNum}/{otp}")
	public String checkOTP(@PathVariable int phoneNum, @PathVariable int otp) {
		return service.checkOTP(phoneNum,otp);
	}
}
